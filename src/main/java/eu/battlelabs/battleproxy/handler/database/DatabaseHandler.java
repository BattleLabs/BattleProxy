package eu.battlelabs.battleproxy.handler.database;

import eu.battlelabs.battleproxy.handler.source.DataSourceHandler;
import net.md_5.bungee.config.Configuration;

public interface DatabaseHandler {

    /**
     * Stellt eine Verbindung mit Datenbank auf. Diese Datenbank kann eine NoSQL oder SQL Datenbank
     * sein.
     *
     * @throws Exception Wirft ein Fehler raus, der beim Abrufen der Verbindung auftreten kann. Diese Exception
     *                   wird zum Beispiel geworfen, wenn keine Verbindung frei ist.
     *
     * @return Wirft ein Objekt zurück, dass eine Verbindung ist.
     */
    Object getConnection() throws Exception;

    /**
     * Wirft ein Objekt raus, wo alle Daten für eine Verbindung zu einer Datenbank gespeichert sind.
     * Diese Daten kann man nicht mehr ändern.
     *
     * @return In diesem Objekt sind Daten für eine Verbindung zu einer Datenbank.
     */
    DataSourceHandler getDatabaseProperties();

    /**
     * Wirft ein Objekt raus, das als Config gespeichert ist. In dieser Config sind die Daten
     * der Datenbank gespeichert.
     *
     * @return Die Config, wo die Daten der Datenbank stehen.
     */
    Configuration getConfig();

}
