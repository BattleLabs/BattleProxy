package eu.battlelabs.battleproxy.handler.database;

import com.zaxxer.hikari.HikariDataSource;
import eu.battlelabs.battleproxy.handler.source.DataSourceHandler;
import eu.battlelabs.battleproxy.objects.source.SQLSource;
import net.md_5.bungee.config.Configuration;

import java.sql.Connection;
import java.sql.SQLException;

public class MySQLHandler implements DatabaseHandler {

    private final SQLSource sqlSource;
    private final HikariDataSource hikariDataSource;
    private Configuration configuration;

    public MySQLHandler(Configuration configuration) {
        this.configuration = configuration;

        sqlSource = new SQLSource(
                configuration.getString("MySQL." + "host"),
                configuration.getInt("MySQL." + "port"),
                configuration.getString("MySQL." + "username"),
                configuration.getString("MySQL." + "password")
        );

        hikariDataSource = new HikariDataSource();

        hikariDataSource.setMaximumPoolSize(10);
        hikariDataSource.setDataSourceClassName("org.mariadb.jdbc.MariaDbDataSource");
        hikariDataSource.addDataSourceProperty("serverName", getDatabaseProperties().getHost());
        hikariDataSource.addDataSourceProperty("port", getDatabaseProperties().getPort());
        hikariDataSource.addDataSourceProperty("databaseName", getDatabaseProperties().getDatabase());
        hikariDataSource.addDataSourceProperty("user", getDatabaseProperties().getUsername());
        hikariDataSource.addDataSourceProperty("password", getDatabaseProperties().getPassword());
    }

    public void executeUpdate(String query) {
        try {
            ((Connection) getConnection()).prepareStatement(query).executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        return hikariDataSource.getConnection();
    }

    @Override
    public DataSourceHandler getDatabaseProperties() {
        return sqlSource;
    }

    @Override
    public Configuration getConfig() {
        return configuration;
    }
}
