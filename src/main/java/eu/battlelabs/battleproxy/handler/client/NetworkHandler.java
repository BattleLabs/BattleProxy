package eu.battlelabs.battleproxy.handler.client;

import eu.battlelabs.battleproxy.BattleProxy;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class NetworkHandler extends SimpleChannelInboundHandler<ByteBuf> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ByteBuf msg) throws Exception {
        String[] args = BattleProxy.getInstance().getNettyClient().readString(msg).split(" ");
        String cmd = args[0].toLowerCase();

        if (cmd.equalsIgnoreCase("end")) {
            String message = args[1];
            BattleProxy.getInstance().getProxy().stop(message);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }
}
