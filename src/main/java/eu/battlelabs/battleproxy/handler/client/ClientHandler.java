package eu.battlelabs.battleproxy.handler.client;

import com.google.common.base.Charsets;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.epoll.Epoll;

public abstract class ClientHandler {

    private String host;
    private int port;

    public ClientHandler(int port) {
        this("127.0.0.1", port);
    }

    public ClientHandler(String host, int port) {
        this.host = host; this.port = port;
    }

    public abstract void setupServerConnection();

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public boolean epollAvailable() {
        return Epoll.isAvailable();
    }

    public String readString(ByteBuf buf) {
        buf.readInt();
        int length = buf.readInt();
        byte[] byteArray = new byte[length];
        buf.readBytes(byteArray);
        buf.resetReaderIndex();

        return new String(byteArray, Charsets.UTF_8);
    }

    public void sendString(String value, Channel channel) {
        ByteBuf buf = Unpooled.buffer();
        byte[] byteArray = value.getBytes(Charsets.UTF_8);
        buf.writeInt(byteArray.length);
        buf.writeBytes(byteArray);
        channel.writeAndFlush(buf);
    }

}
