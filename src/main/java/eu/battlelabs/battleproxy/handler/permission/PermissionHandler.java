package eu.battlelabs.battleproxy.handler.permission;

import eu.battlelabs.battleproxy.BattleProxy;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PermissionHandler {

    private File fileConfig;
    private Configuration bungeeConfig;

    public PermissionHandler() {
        try {
            fileConfig = new File("config.yml");
            bungeeConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(fileConfig);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public void createGroup(String group) throws IOException {
        bungeeConfig.set("permissions." + group, new ArrayList<String>());

        ConfigurationProvider.getProvider(YamlConfiguration.class).save(bungeeConfig, fileConfig);
        BungeeCord.getInstance().config.load();
    }

    public void deleteGroup(String group) throws IOException {
        bungeeConfig.set("permissions." + group, null);

        ConfigurationProvider.getProvider(YamlConfiguration.class).save(bungeeConfig, fileConfig);
        BungeeCord.getInstance().config.load();
    }

    public void clearUser(String player) throws IOException {
        bungeeConfig.set("groups." + player, null);

        ConfigurationProvider.getProvider(YamlConfiguration.class).save(bungeeConfig, fileConfig);
        BungeeCord.getInstance().config.load();
    }

    public void addPermission(String group, String permission) throws IOException {

        List<String> permissions = bungeeConfig.getStringList("permissions." + group);
        permissions.add(permission);
        bungeeConfig.set("permissions." + group, permissions);

        ConfigurationProvider.getProvider(YamlConfiguration.class).save(bungeeConfig, fileConfig);
        BungeeCord.getInstance().config.load();

        for (ProxiedPlayer proxiedPlayer : BattleProxy.getInstance().getProxy().getPlayers()) {
            if (proxiedPlayer.getGroups().contains(group)) {
                proxiedPlayer.setPermission(permission, true);
            }
        }
    }

    public void addGroup(String player, String group) throws IOException {
        if (BattleProxy.getInstance().getProxy().getPlayer(player) != null) {
            ProxiedPlayer proxiedPlayer = BattleProxy.getInstance().getProxy().getPlayer(player);
            proxiedPlayer.addGroups(group);
        }


        List<String> groups = bungeeConfig.getStringList("groups." + player);
        groups.add(group);
        bungeeConfig.set("groups." + player, groups);

        ConfigurationProvider.getProvider(YamlConfiguration.class).save(bungeeConfig, fileConfig);
        BungeeCord.getInstance().config.load();
    }

    public void removePermission(String group, String permission) throws IOException {

        List<String> permissions = bungeeConfig.getStringList("permissions." + group);
        permissions.remove(permission);
        bungeeConfig.set("permissions." + group, permissions);

        ConfigurationProvider.getProvider(YamlConfiguration.class).save(bungeeConfig, fileConfig);
        BungeeCord.getInstance().config.load();

        for (ProxiedPlayer proxiedPlayer : BattleProxy.getInstance().getProxy().getPlayers()) {
            if (proxiedPlayer.getGroups().contains(group)) {
                proxiedPlayer.setPermission(permission, false);
            }
        }
    }

    public void removeGroup(String player, String group) throws IOException {
        if (BattleProxy.getInstance().getProxy().getPlayer(player) != null) {
            ProxiedPlayer proxiedPlayer = BattleProxy.getInstance().getProxy().getPlayer(player);
            proxiedPlayer.removeGroups(group);
        }

        List<String> groups = bungeeConfig.getStringList("groups." + player);
        groups.remove(group);
        bungeeConfig.set("groups." + player, groups);

        ConfigurationProvider.getProvider(YamlConfiguration.class).save(bungeeConfig, fileConfig);
        BungeeCord.getInstance().config.load();
    }

    public boolean groupExists(String group) {
        return bungeeConfig.contains("permissions." + group);
    }

    public boolean userExsits(String player) {
        return bungeeConfig.contains("groups." + player);
    }
}
