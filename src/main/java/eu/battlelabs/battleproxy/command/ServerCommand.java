package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TextType;
import eu.battlelabs.battleproxy.objects.proxy.functional.ServerPing;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Map;

public class ServerCommand extends Command {

    private String usage = "/server {Server}";

    public ServerCommand() {
        super("server");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)cs;

            if (proxiedPlayer.hasPermission(Permissions.SERVER_MAIN.getPermission())) {
                if (args.length == 0) {
                    Map<String, ServerInfo> servers = ProxyServer.getInstance().getServers();

                    if (servers.isEmpty()) {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Es sind keine Server eingetragen.");
                        return;
                    }

                    String serverList = "";

                    proxiedPlayer.sendMessage(TextType.HEADER.getText());
                    proxiedPlayer.sendMessage("");
                    proxiedPlayer.sendMessage("§7Du befindest dich zurzeit auf dem Server: §b" + proxiedPlayer.getServer().getInfo().getName());
                    proxiedPlayer.sendMessage("");
                    proxiedPlayer.sendMessage("§7Aktuell registrierte Server:");

                    for (String server : servers.keySet()) {
                        ServerPing ping = new ServerPing(servers.get(server).getAddress().getHostName(), servers.get(server).getAddress().getPort());

                        if (ping.parseData(ServerPing.Connection.MOTD) != null) {
                            serverList = serverList + " §a" + servers.get(server).getName();
                        } else{
                            serverList = serverList + " §c" + servers.get(server).getName();
                        }
                    }
                    proxiedPlayer.sendMessage(serverList);

                    proxiedPlayer.sendMessage("");
                    proxiedPlayer.sendMessage(TextType.HEADER.getText());
                } else if (args.length == 1) {
                    if (!proxiedPlayer.hasPermission(Permissions.SERVER_CONNECT.getPermission())) {
                        proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                        return;
                    }
                    String server = args[0];

                    if (!BattleProxy.getInstance().getProxy().getServers().containsKey(server)) {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDer angegebene Server ist unbekannt.");
                        return;
                    }

                    if (proxiedPlayer.getServer().getInfo().getAddress().getPort() == BattleProxy.getInstance().getProxy().getServerInfo(server).getAddress().getPort()) {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDu bist bereits mit dem Server verbunden.");
                        return;
                    }

                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verbinde mit Server §b" + server + "§7...");
                    proxiedPlayer.connect(BattleProxy.getInstance().getProxy().getServerInfo(server));

                } else {
                    proxiedPlayer.sendMessage(TextType.PREFIX + "Verwendung: §b" + usage);
                }
            } else {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
            }
        } else {
            cs.sendMessage(TextType.PREFIX.getText() + "§cDieser Befehl ist nur für Spieler gedacht.");
        }
    }
}
