package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TextType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class SendCommand extends Command {

    private String usage = "/send {User} {Server}";

    public SendCommand() {
        super("send");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)cs;

            if (proxiedPlayer.hasPermission(Permissions.SEND_MAIN.getPermission())) {
                if (args.length == 2) {
                    String name = args[0];
                    String server = args[1];

                    if (BattleProxy.getInstance().getProxy().getPlayer(name) == null) {
                        proxiedPlayer.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return;
                    }

                    if (!BattleProxy.getInstance().getProxy().getServers().containsKey(server)) {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDieser Server ist unbekannt.");
                        return;
                    }

                    ProxiedPlayer target = BattleProxy.getInstance().getProxy().getPlayer(name);

                    target.sendMessage(TextType.PREFIX.getText() + "Verbinde mit Server §b" + target.getName() + "§7...");
                    target.connect(BattleProxy.getInstance().getProxy().getServers().get(server));
                } else {
                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
            }
        } else {
            if (args.length == 2) {
                String name = args[0];
                String server = args[1];

                if (BattleProxy.getInstance().getProxy().getPlayer(name) == null) {
                    cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                    return;
                }

                if (!BattleProxy.getInstance().getProxy().getServers().containsKey(server)) {
                    cs.sendMessage(TextType.PREFIX.getText() + "§cDieser Server ist unbekannt.");
                    return;
                }

                ProxiedPlayer target = BattleProxy.getInstance().getProxy().getPlayer(name);

                target.sendMessage(TextType.PREFIX.getText() + "Verbinde mit Server §b" + target.getName() + "§7...");
                target.connect(BattleProxy.getInstance().getProxy().getServers().get(server));
            }
        }
    }
}
