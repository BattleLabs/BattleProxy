package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TablesType;
import eu.battlelabs.battleproxy.constant.TextType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.SQLException;

public class MaintenanceCommand extends Command {

    private String usage = "/maintenance <status/toggle>";

    public MaintenanceCommand() {
        super("maintenance");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)cs;

            if (proxiedPlayer.hasPermission(Permissions.MAINTENANCE_MAIN.getPermission())) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("status")) {
                        if (BattleProxy.maintenance) {
                            proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Wartungsmodus: §aaktiviert");
                        } else {
                            proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Wartungsmodus: §cdeaktiviert");
                        }
                    } else if (args[0].equalsIgnoreCase("toggle")) {
                        if (!proxiedPlayer.hasPermission(Permissions.MAINTENANCE_TOGGLE.getPermission())) {
                            proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                            return;
                        }

                        try {
                            if (BattleProxy.maintenance) {
                                BattleProxy.maintenance = false;
                                BattleProxy.getInstance().getProxyData().updateData("motd", false, TablesType.PROXY_DATA);
                            } else {
                                BattleProxy.maintenance = true;
                                BattleProxy.getInstance().getProxyData().updateData("motd", true, TablesType.PROXY_DATA);

                                for (ProxiedPlayer player : BattleProxy.getInstance().getProxy().getPlayers()) {
                                    if (!player.hasPermission(Permissions.MAINTENANCE_BYPASS.getPermission())) {
                                        player.disconnect("§b§lBattleLabsEU \n" +
                                                "\n" +
                                                "§cDer Server ist nun in Wartungsarbeiten. Versuch es später nochmal.");
                                    }
                                }

                                BattleProxy.getInstance().getProxy().broadcast(TextType.PREFIX.getText() + "§cDer Server ist nun in Wartungsarbreiten.");
                            }
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
            }
        } else {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("status")) {
                    if (BattleProxy.maintenance) {
                        cs.sendMessage(TextType.PREFIX.getText() + "Wartungsmodus: §aaktiviert");
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "Wartungsmodus: §cdeaktiviert");
                    }
                } else if (args[0].equalsIgnoreCase("toggle")) {
                    try {
                        if (BattleProxy.maintenance) {
                            BattleProxy.maintenance = false;
                            BattleProxy.getInstance().getProxyData().updateData("motd", false, TablesType.PROXY_DATA);
                        } else {
                            BattleProxy.maintenance = true;
                            BattleProxy.getInstance().getProxyData().updateData("motd", true, TablesType.PROXY_DATA);

                            for (ProxiedPlayer player : BattleProxy.getInstance().getProxy().getPlayers()) {
                                if (!player.hasPermission(Permissions.MAINTENANCE_BYPASS.getPermission())) {
                                    player.disconnect("§b§lBattleLabsEU \n" +
                                            "\n" +
                                            "§cDer Server ist nun in Wartungsarbeiten. Versuch es später nochmal.");
                                }
                            }

                            BattleProxy.getInstance().getProxy().broadcast(TextType.PREFIX.getText() + "§cDer Server ist nun in Wartungsarbreiten.");
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
            }
        }
    }
}
