package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TextType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ListCommand extends Command {

    public ListCommand() {
        super("plist");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)cs;

            if (!proxiedPlayer.hasPermission(Permissions.LIST_MAIN.getPermission())) {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                return;
            }

            proxiedPlayer.sendMessage(TextType.HEADER.getText());
            proxiedPlayer.sendMessage("");

            for (ServerInfo serverInfo : BattleProxy.getInstance().getProxy().getServers().values()) {
                proxiedPlayer.sendMessage("§8>> §b" + serverInfo.getName());

                for (ProxiedPlayer player : serverInfo.getPlayers()) {
                    proxiedPlayer.sendMessage("    §a" + player.getName());
                }
            }

            proxiedPlayer.sendMessage("");
            proxiedPlayer.sendMessage(TextType.HEADER.getText());
        } else {
            cs.sendMessage(TextType.HEADER.getText());
            cs.sendMessage("");

            for (ServerInfo serverInfo : BattleProxy.getInstance().getProxy().getServers().values()) {
                cs.sendMessage("§8>> §b" + serverInfo.getName());

                for (ProxiedPlayer player : serverInfo.getPlayers()) {
                    cs.sendMessage("    §a" + player.getName());
                }
            }

            cs.sendMessage("");
            cs.sendMessage(TextType.HEADER.getText());
        }
    }
}
