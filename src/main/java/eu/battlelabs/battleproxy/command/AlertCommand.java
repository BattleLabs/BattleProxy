package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TextType;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class AlertCommand extends Command {

    private String usage = "/alert <Nachricht>";

    public AlertCommand() {
        super("alert");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)cs;

            if (proxiedPlayer.hasPermission(Permissions.ALERT_MAIN.getPermission())) {
                if (args.length >= 1) {
                    String msg = "";

                    for (int i = 0; i < args.length; i++) {
                        msg = msg + " " + args[i];
                    }

                    BattleProxy.getInstance().getProxy().broadcast(ChatColor.translateAlternateColorCodes('&', msg));

                } else {
                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
            }
        } else {
            if (args.length >= 1) {
                String msg = "";

                for (int i = 0; i < args.length; i++) {
                    msg = msg + " " + args[i];
                }

                BattleProxy.getInstance().getProxy().broadcast(ChatColor.translateAlternateColorCodes('&', msg));

            } else {
                cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
            }
        }
    }
}
