package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TextType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class FindCommand extends Command {

    private String usage = "/find {User}";

    public FindCommand() {
        super("find");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)cs;

            if (proxiedPlayer.hasPermission(Permissions.FIND_MAIN.getPermission())) {
                if (args.length == 1) {
                    String name = args[0];

                    if (BattleProxy.getInstance().getProxy().getPlayer(name) == null) {
                        proxiedPlayer.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                        return;
                    }

                    ProxiedPlayer target = BattleProxy.getInstance().getProxy().getPlayer(name);

                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verbinde mit Server §b" + target.getServer().getInfo() + "§7...");
                    proxiedPlayer.connect(target.getServer().getInfo());
                } else {
                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
            }
        } else {
            if (args.length == 1) {
                String name = args[0];

                if (BattleProxy.getInstance().getProxy().getPlayer(name) == null) {
                    cs.sendMessage(TextType.PLAYER_NOT_FOUND.getText());
                    return;
                }

                ProxiedPlayer target = BattleProxy.getInstance().getProxy().getPlayer(name);
                cs.sendMessage(TextType.PREFIX.getText() + "§b" + name + " befindet sich auf dem Server §b" + target.getServer().getInfo() + "§7.");
            }
        }
    }
}
