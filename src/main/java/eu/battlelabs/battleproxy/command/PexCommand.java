package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TextType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.IOException;

public class PexCommand extends Command {

    private String usage = "/proxypex user {User} <add/remove> <Group> " +
            "\n " +
                           "/proxypex user {User} clear" +
            "\n " +
                           "/proxypex group {Group} <add/remove> {Permission>" +
            "\n " +
                           "/proxypex group <Group> <create/delete>";

    public PexCommand() {
        super("proxypex");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer) cs;

            if (proxiedPlayer.hasPermission(Permissions.PEX_MAIN.getPermission())) {
                if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("group")) {
                        String group = args[1];

                        if (args[2].equalsIgnoreCase("create")) {
                            if (!proxiedPlayer.hasPermission(Permissions.PEX_GROUP_CREATE.getPermission())) {
                                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                                return;
                            }

                            if (BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert bereits.");
                                return;
                            }

                            try {
                                BattleProxy.getInstance().getPermissionHandler().createGroup(group);
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Die Proxy Gruppe §b" + group + " §7wurde erfolgreich erstellt.");
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }

                        } else if (args[2].equalsIgnoreCase("delete")) {
                            if (!proxiedPlayer.hasPermission(Permissions.PEX_GROUP_DELETE.getPermission())) {
                                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                                return;
                            }

                            if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert bereits.");
                                return;
                            }

                            try {
                                BattleProxy.getInstance().getPermissionHandler().deleteGroup(group);
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Die Proxy Gruppe §b" + group + " §7wurde erfolgreich gelöscht.");
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }

                        } else {
                            proxiedPlayer.sendMessage(TextType.PREFIX.getText()+ "Verwendung: §b" + usage);
                        }
                    } else if (args[0].equalsIgnoreCase("user")) {
                        String user = args[1];

                        if (args[2].equalsIgnoreCase("clear")) {
                            if (!proxiedPlayer.hasPermission(Permissions.PEX_USER_CLEAR.getPermission())) {
                                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                                return;
                            }

                            if (!BattleProxy.getInstance().getPermissionHandler().userExsits(user)) {
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDieser Spieler besitzt keine Proxy-Gruppen.");
                                return;
                            }

                            try {
                                BattleProxy.getInstance().getPermissionHandler().clearUser(user);

                                if (BattleProxy.getInstance().getProxy().getPlayer(user) != null) {
                                    BattleProxy.getInstance().getProxy().getPlayer(user).sendMessage(TextType.PREFIX.getText() + "Du besitzt nun keine Proxy-Gruppen.");
                                }

                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§b" + user + " §7hat keine Proxy-Gruppen mehr.");
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        } else {
                            proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                        }
                    } else {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else if (args.length == 4) {
                    if (args[0].equalsIgnoreCase("user")) {
                        String user = args[1];

                        if (args[2].equalsIgnoreCase("add")) {
                            if (!proxiedPlayer.hasPermission(Permissions.PEX_USER_ADD.getPermission())) {
                                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                                return;
                            }

                            String group = args[3];

                            if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                                return;
                            }

                            try {
                                BattleProxy.getInstance().getPermissionHandler().addGroup(user, group);

                                if (BattleProxy.getInstance().getProxy().getPlayer(user) != null) {
                                    BattleProxy.getInstance().getProxy().getPlayer(user).sendMessage(TextType.PREFIX.getText() + "Du bist nun in der Proxy Gruppe §b" + group + "§7.");
                                }

                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§b" + user + " §7ist nun in der Proxy Gruppe §b" + group + "§7.");
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        } else if (args[2].equalsIgnoreCase("remove")) {
                            if (!proxiedPlayer.hasPermission(Permissions.PEX_USER_REMOVE.getPermission())) {
                                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                                return;
                            }

                            String group = args[3];

                            if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                                return;
                            }

                            try {
                                BattleProxy.getInstance().getPermissionHandler().removeGroup(user, group);

                                if (BattleProxy.getInstance().getProxy().getPlayer(user) != null) {
                                    BattleProxy.getInstance().getProxy().getPlayer(user).sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr in der Proxy Gruppe §b" + group + "§7.");
                                }

                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§b" + user + " §7ist nun nicht mehr in der Proxy Gruppe §b" + group + "§7.");
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        } else {
                            proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                        }
                    } else if (args[0].equalsIgnoreCase("group")) {
                        String group = args[1];

                        if (args[2].equalsIgnoreCase("add")) {
                            if (!proxiedPlayer.hasPermission(Permissions.PEX_GROUP_ADD.getPermission())) {
                                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                                return;
                            }

                            String permission = args[3];

                            if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                                return;
                            }

                            try {
                                BattleProxy.getInstance().getPermissionHandler().addPermission(group, permission);
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§7Die Permission §b" + permission + " §7wurde zu der Proxy Gruppe §b" + group + " §7übertragen.");
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        } else if (args[2].equalsIgnoreCase("remove")) {
                            if (!proxiedPlayer.hasPermission(Permissions.PEX_GROUP_REMOVE.getPermission())) {
                                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                                return;
                            }

                            String permission = args[3];

                            if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                                return;
                            }

                            try {
                                BattleProxy.getInstance().getPermissionHandler().removePermission(group, permission);
                                proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "§7Die Permission §b" + permission + " §7wurde von der Proxy Gruppe §b" + group + " §7weggenommen.");
                            } catch (IOException ex) {
                                ex.printStackTrace();
                            }
                        } else {
                            proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                        }
                    } else {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText()+ "Verwendung: §b" + usage);
                    }
                } else {
                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
            }
        } else {
            if (args.length == 3) {
                if (args[0].equalsIgnoreCase("group")) {
                    String group = args[1];

                    if (args[2].equalsIgnoreCase("create")) {
                        if (BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert bereits.");
                            return;
                        }

                        try {
                            BattleProxy.getInstance().getPermissionHandler().createGroup(group);
                            cs.sendMessage(TextType.PREFIX.getText() + "Die Proxy Gruppe §b" + group + " §7wurde erfolgreich erstellt.");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    } else if (args[2].equalsIgnoreCase("delete")) {
                        if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert bereits.");
                            return;
                        }

                        try {
                            BattleProxy.getInstance().getPermissionHandler().deleteGroup(group);
                            cs.sendMessage(TextType.PREFIX.getText() + "Die Proxy Gruppe §b" + group + " §7wurde erfolgreich gelöscht.");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }

                    } else {
                        cs.sendMessage(TextType.PREFIX.getText()+ "Verwendung: §b" + usage);
                    }
                } else if (args[0].equalsIgnoreCase("user")) {
                    String user = args[1];

                    if (args[2].equalsIgnoreCase("clear")) {
                        if (!BattleProxy.getInstance().getPermissionHandler().userExsits(user)) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cDieser Spieler besitzt keine Proxy-Gruppen.");
                            return;
                        }

                        try {
                            BattleProxy.getInstance().getPermissionHandler().clearUser(user);

                            if (BattleProxy.getInstance().getProxy().getPlayer(user) != null) {
                                BattleProxy.getInstance().getProxy().getPlayer(user).sendMessage(TextType.PREFIX.getText() + "Du besitzt nun keine Proxy-Gruppen.");
                            }

                            cs.sendMessage(TextType.PREFIX.getText() + "§b" + user + " §7hat keine Proxy-Gruppen mehr.");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else if (args.length == 4) {
                if (args[0].equalsIgnoreCase("user")) {
                    String user = args[1];

                    if (args[2].equalsIgnoreCase("add")) {
                        String group = args[3];

                        if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                            return;
                        }

                        try {
                            BattleProxy.getInstance().getPermissionHandler().addGroup(user, group);

                            if (BattleProxy.getInstance().getProxy().getPlayer(user) != null) {
                                BattleProxy.getInstance().getProxy().getPlayer(user).sendMessage(TextType.PREFIX.getText() + "Du bist nun in der Proxy Gruppe §b" + group + "§7.");
                            }

                            cs.sendMessage(TextType.PREFIX.getText() + "§b" + user + " §7ist nun in der Proxy Gruppe §b" + group + "§7.");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else if (args[2].equalsIgnoreCase("remove")) {
                        String group = args[3];

                        if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                            return;
                        }

                        try {
                            BattleProxy.getInstance().getPermissionHandler().removeGroup(user, group);

                            if (BattleProxy.getInstance().getProxy().getPlayer(user) != null) {
                                BattleProxy.getInstance().getProxy().getPlayer(user).sendMessage(TextType.PREFIX.getText() + "Du bist nun nicht mehr in der Proxy Gruppe §b" + group + "§7.");
                            }

                            cs.sendMessage(TextType.PREFIX.getText() + "§b" + user + " §7ist nun nicht mehr in der Proxy Gruppe §b" + group + "§7.");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else if (args[0].equalsIgnoreCase("group")) {
                    String group = args[1];

                    if (args[2].equalsIgnoreCase("add")) {
                        String permission = args[3];

                        if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                            return;
                        }

                        try {
                            BattleProxy.getInstance().getPermissionHandler().addPermission(group, permission);
                            cs.sendMessage(TextType.PREFIX.getText() + "§7Die Permission §b" + permission + " §7wurde zu der Proxy Gruppe §b" + group + " §7übertragen.");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else if (args[2].equalsIgnoreCase("remove")) {
                        String permission = args[3];

                        if (!BattleProxy.getInstance().getPermissionHandler().groupExists(group)) {
                            cs.sendMessage(TextType.PREFIX.getText() + "§cDiese Proxy Gruppe existiert nicht.");
                            return;
                        }

                        try {
                            BattleProxy.getInstance().getPermissionHandler().removePermission(group, permission);
                            cs.sendMessage(TextType.PREFIX.getText() + "§7Die Permission §b" + permission + " §7wurde von der Proxy Gruppe §b" + group + " §7weggenommen.");
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    cs.sendMessage(TextType.PREFIX.getText()+ "Verwendung: §b" + usage);
                }
            } else {
                cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
            }
        }
    }
}
