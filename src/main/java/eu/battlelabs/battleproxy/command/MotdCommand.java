package eu.battlelabs.battleproxy.command;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import eu.battlelabs.battleproxy.constant.TextType;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MotdCommand extends Command {

    private String usage = "/motd <update/get>";

    public MotdCommand() {
        super("motd");
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (cs instanceof ProxiedPlayer) {
            ProxiedPlayer proxiedPlayer = (ProxiedPlayer)cs;

            if (proxiedPlayer.hasPermission(Permissions.MOTD_MAIN.getPermission())) {
                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("update")) {
                        if (!proxiedPlayer.hasPermission(Permissions.MOTD_UPDATE.getPermission())) {
                            proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
                            return;
                        }

                        try {
                            PreparedStatement preparedStatement = BattleProxy.getInstance().getMySQLHandler().getConnection().prepareStatement("SELECT * FROM " + "battlelabs_proxy_data");
                            ResultSet resultSet = preparedStatement.executeQuery();

                            while (resultSet.next()) {
                                BattleProxy.maintenance = resultSet.getBoolean("maintenance");
                                BattleProxy.motd = resultSet.getString("motd");
                            }

                            preparedStatement.close();
                            resultSet.close();
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        }

                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Die Motd wurde erfolgreich neugeladen.");
                    } else if (args[0].equalsIgnoreCase("get")) {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Motd: §b" + BattleProxy.motd);
                    } else {
                        proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                    }
                } else {
                    proxiedPlayer.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                proxiedPlayer.sendMessage(TextType.NO_PERMISSION.getText());
            }
        } else {
            if (args.length == 1) {
                if (args[0].equalsIgnoreCase("update")) {

                    try {
                        PreparedStatement preparedStatement = BattleProxy.getInstance().getMySQLHandler().getConnection().prepareStatement("SELECT * FROM " + "battlelabs_proxy_data");
                        ResultSet resultSet = preparedStatement.executeQuery();

                        while (resultSet.next()) {
                            BattleProxy.maintenance = resultSet.getBoolean("maintenance");
                            BattleProxy.motd = resultSet.getString("motd");
                        }

                        preparedStatement.close();
                        resultSet.close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }

                    cs.sendMessage(TextType.PREFIX.getText() + "Die Motd wurde erfolgreich neugeladen.");
                } else if (args[0].equalsIgnoreCase("get")) {
                    cs.sendMessage(TextType.PREFIX.getText() + "Motd: §b" + BattleProxy.motd);
                } else {
                    cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
                }
            } else {
                cs.sendMessage(TextType.PREFIX.getText() + "Verwendung: §b" + usage);
            }
        }
    }
}
