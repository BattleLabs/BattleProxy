package eu.battlelabs.battleproxy.objects.client;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.handler.client.ClientHandler;
import eu.battlelabs.battleproxy.handler.client.NetworkHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.LengthFieldPrepender;

import java.util.logging.Level;

public class NettyClient extends ClientHandler {

    private static Channel channel;

    public NettyClient(int port) {
        super("127.0.0.1", port);
    }

    public NettyClient(String host, int port) {
        super(host, port);
    }

    @Override
    public void setupServerConnection() {
        EventLoopGroup workerGroup = this.epollAvailable() ? new EpollEventLoopGroup() : new NioEventLoopGroup();

        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(workerGroup)
                    .channel(this.epollAvailable() ? EpollSocketChannel.class : NioSocketChannel.class)
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel ch) throws Exception {
                            ch.config().setTrafficClass(0x18);
                            ch.pipeline().addLast(
                                    new LengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 4),
                                    new LengthFieldPrepender(4),
                                    new NetworkHandler()
                            );
                            channel = ch;
                        }
                    });

            ChannelFuture f = bootstrap.connect(getHost(), getPort());

            f.addListener(connFuture -> {
                if (!f.isSuccess()) {
                    BattleProxy.getInstance().getLogger().log(Level.SEVERE, "Es konnte leider keine Verbindung zur Cloud hergestellt werden.");
                    BattleProxy.getInstance().getProxy().stop("Die Proxy wurde beendet, da keine Verbindung gefunden wurde.");
                    return;
                }

                System.out.println("Die Proxy wurde erfolgreich mit der Cloud verbunden.");

                sendString("registerproxy " + BattleProxy.getInstance().getProxyPort(), getChannel());

                f.channel().closeFuture().addListener(closeFuture -> {
                    BattleProxy.getInstance().getLogger().log(Level.INFO, "Die Verbindung mit der Cloud wurde geschloßen!");
                });
            });

            f.sync();
            f.channel().closeFuture().sync();
        } catch (InterruptedException ex) {
            BattleProxy.getInstance().getLogger().log(Level.SEVERE, "ClientHandler konnte nicht initalisiert werden:", ex);
        } finally {
            workerGroup.shutdownGracefully();
        }
    }

    public static Channel getChannel() {
        return channel;
    }
}
