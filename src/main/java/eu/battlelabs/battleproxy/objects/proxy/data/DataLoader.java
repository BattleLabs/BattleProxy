package eu.battlelabs.battleproxy.objects.proxy.data;

import eu.battlelabs.battleproxy.constant.ObjectType;
import eu.battlelabs.battleproxy.constant.TablesType;
import eu.battlelabs.battleproxy.handler.database.MySQLHandler;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class DataLoader {

    private MySQLHandler mySQLHandler;

    public DataLoader(MySQLHandler mySQLHandler) {
        this.mySQLHandler = mySQLHandler;
    }

    public abstract void loadData();

    public abstract Object getData(ResultSet rs, ObjectType obj, String key) throws SQLException;

    public abstract void updateData(String key, Object value, TablesType type) throws SQLException;

    public void dropData(Object search, TablesType type) throws SQLException {
        return;
    }

    public abstract void insertData(String value, TablesType type) throws SQLException;

    public MySQLHandler getMySQLHandler() {
        return mySQLHandler;
    }

}
