package eu.battlelabs.battleproxy.objects.proxy.data;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.ObjectType;
import eu.battlelabs.battleproxy.constant.TablesType;
import eu.battlelabs.battleproxy.handler.database.MySQLHandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProxyData extends DataLoader {

    private static String PROXY_DATA = "battlelabs_proxy_data";

    public ProxyData(MySQLHandler mySQLHandler) {
        super(mySQLHandler);
    }

    @Override
    public void loadData() {
        Connection connection = null;

        try {
            connection = getMySQLHandler().getConnection();

            for (int i = 0; i < 1; i++) {
                if (i == 0) {
                    PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM " + PROXY_DATA);
                    ResultSet resultSet = preparedStatement.executeQuery();

                    while (resultSet.next()) {
                        BattleProxy.maintenance = resultSet.getBoolean("maintenance");
                        BattleProxy.motd = resultSet.getString("motd");
                    }

                    preparedStatement.close();
                    resultSet.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public Object getData(ResultSet rs, ObjectType obj, String key) throws SQLException {
        while (rs.next()) {
            switch (obj) {
                case INTEGER:
                    return rs.getInt(key);
                case STRING:
                    return rs.getString(key);
                case BOOLEAN:
                    return rs.getBoolean(key);
                case LONG:
                    return rs.getLong(key);
                case DOUBLE:
                    return rs.getDouble(key);
            }
        }

        return null;
    }

    @Override
    public void updateData(String key, Object value, TablesType type) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            switch (type) {
                case PROXY_DATA:
                    preparedStatement = connection.prepareStatement("UPDATE " + PROXY_DATA + " SET " + key + "=?");
                    preparedStatement.setObject(1, value);

                    preparedStatement.execute();
                    break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public void insertData(String value, TablesType type) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = getMySQLHandler().getConnection();

            switch (type) {
                case PROXY_DATA:
                    preparedStatement = connection.prepareStatement("INSERT INTO " + PROXY_DATA + " (" + TablesType.PROXY_DATA.getSQLSyntax() + ") VALUES (" + value + ")");
                    preparedStatement.execute();

                    break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    @Override
    public MySQLHandler getMySQLHandler() {
        return super.getMySQLHandler();
    }

    public static void createTable(TablesType type) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleProxy.getInstance().getMySQLHandler().getConnection();

            switch (type) {
                case PROXY_DATA:
                    preparedStatement = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + PROXY_DATA + " (" + type.getTableSyntax() + ")");
                    preparedStatement.execute();

                    break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void deleteTable(TablesType type) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleProxy.getInstance().getMySQLHandler().getConnection();

            switch (type) {
                case PROXY_DATA:
                    preparedStatement = connection.prepareStatement("DROP TABLE IF EXISTS " + PROXY_DATA);
                    preparedStatement.execute();

                    break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public static void clearTable(TablesType type) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = BattleProxy.getInstance().getMySQLHandler().getConnection();

            switch (type) {
                case PROXY_DATA:
                    preparedStatement = connection.prepareStatement("TRUNCATE " + PROXY_DATA);
                    preparedStatement.execute();

                    break;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
