package eu.battlelabs.battleproxy.constant;

public enum ObjectType {

    INTEGER,
    STRING,
    BOOLEAN,
    LONG,
    DOUBLE;

    public static boolean isInteger(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

}
