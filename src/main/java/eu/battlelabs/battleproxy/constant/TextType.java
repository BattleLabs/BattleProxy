package eu.battlelabs.battleproxy.constant;

public enum TextType {

    PREFIX("§b§lBattleProxy §8» §7"),
    HEADER("§8<§7§m--------------§8▎§b§l BattleLabs §8▎§7§m--------------§8>"),
    NO_PERMISSION(PREFIX.getText() + "§cDu hast nicht die Berechtigung diesen Befehl auszuführen."),
    PLAYER_NOT_FOUND(PREFIX.getText() + "§cDieser Spieler ist offline oder existiert nicht.");

    private String text;

    TextType(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
