package eu.battlelabs.battleproxy.constant;

public enum Permissions {

    SERVER_MAIN("battlelabs.proxy.server"),
    SERVER_CONNECT("battlelabs.proxy.server.connect"),
    PEX_MAIN("battlelabs.proxy.pex"),
    PEX_USER_ADD("battlelabs.proxy.pex.user.add"),
    PEX_USER_REMOVE("battlelabs.proxy.pex.user.remove"),
    PEX_USER_CLEAR("battlelabs.proxy.pex.user.clear"),
    PEX_GROUP_ADD("battlelabs.proxy.pex.group.add"),
    PEX_GROUP_REMOVE("battlelabs.proxy.pex.group.remove"),
    PEX_GROUP_CREATE("battlelabs.proxy.pex.group.create"),
    PEX_GROUP_DELETE("battlelabs.proxy.pex.group.delete"),
    ALERT_MAIN("battlelabs.proxy.alert"),
    LIST_MAIN("battlelabs.proxy.list"),
    FIND_MAIN("battlelabs.proxy.find"),
    SEND_MAIN("battlelabs.proxy.send"),
    MOTD_MAIN("battlelabs.proxy.motd"),
    MOTD_UPDATE("battlelabs.proxy.motd.update"),
    MAINTENANCE_MAIN("battlelabs.proxy.maintenance"),
    MAINTENANCE_TOGGLE("battlelabs.proxy.maintenance.toggle"),
    MAINTENANCE_BYPASS("battlelabs.proxy.maintenance.bypass");

    private String permission;

    Permissions(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
