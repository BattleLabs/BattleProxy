package eu.battlelabs.battleproxy.constant;

public enum TablesType {

    PROXY_DATA("maintenance, motd", "maintenance boolean, motd text"),
    PLAYER_DATA("id, uuid, playername, tokens, votepoints, lastip, playtime, lastplaytime, kills, deaths", "id int, uuid text, playername text, tokens int, votepoints int, lastip text, playtime long, lastplaytime date, kill int, deaths int");

    private String sqlSyntax;
    private String tableSyntax;

    TablesType(String sqlSyntax, String tableSyntax) {
        this.sqlSyntax = sqlSyntax;
        this.tableSyntax = tableSyntax;
    }

    public String getSQLSyntax() {
        return sqlSyntax;
    }

    public String getTableSyntax() {
        return tableSyntax;
    }
}
