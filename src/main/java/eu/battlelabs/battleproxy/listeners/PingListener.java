package eu.battlelabs.battleproxy.listeners;

import eu.battlelabs.battleproxy.BattleProxy;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ServerPing;
import net.md_5.bungee.api.event.ProxyPingEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PingListener implements Listener {

    @EventHandler
    public void onPing(ProxyPingEvent e) {
        String motd = ChatColor.translateAlternateColorCodes('&', BattleProxy.motd);
        ServerPing ping = e.getResponse();
        ping.setDescription(motd);

        if (BattleProxy.maintenance) {
            ping.setVersion(new ServerPing.Protocol("§cWartungsarbeiten", 0));
        }

        e.setResponse(ping);
    }

}
