package eu.battlelabs.battleproxy.listeners;

import eu.battlelabs.battleproxy.BattleProxy;
import eu.battlelabs.battleproxy.constant.Permissions;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class JQListener implements Listener {

    @EventHandler
    public void onJoin(ServerConnectEvent e) {

        ProxiedPlayer proxiedPlayer = e.getPlayer();

        if (BattleProxy.maintenance) {
            if (!proxiedPlayer.hasPermission(Permissions.MAINTENANCE_BYPASS.getPermission())) {
                proxiedPlayer.disconnect("§b§lBattleLabsEU \n" +
                        "\n" +
                        "§7Wir sind zurzeit in Wartungsarbeiten, \n" +
                        "§7versuch es später nochmal.");
            }
        }

    }

}
