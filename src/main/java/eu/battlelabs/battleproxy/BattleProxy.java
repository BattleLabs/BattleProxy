package eu.battlelabs.battleproxy;

import com.google.common.io.ByteStreams;
import eu.battlelabs.battleproxy.command.*;
import eu.battlelabs.battleproxy.constant.TablesType;
import eu.battlelabs.battleproxy.handler.database.MySQLHandler;
import eu.battlelabs.battleproxy.handler.permission.PermissionHandler;
import eu.battlelabs.battleproxy.listeners.JQListener;
import eu.battlelabs.battleproxy.listeners.PingListener;
import eu.battlelabs.battleproxy.objects.client.NettyClient;
import eu.battlelabs.battleproxy.objects.proxy.data.ProxyData;
import eu.battlelabs.battleproxy.threads.ServerUpdater;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.*;
import java.util.concurrent.TimeUnit;

public class BattleProxy extends Plugin {

    private static BattleProxy instance;
    private NettyClient nettyClient;
    private Configuration configuration;
    private MySQLHandler mysqlHandler;
    private PermissionHandler permissionHandler;
    private ProxyData proxyData;
    private int PROXY_PORT;
    public static String motd = "Line1\nLine2";
    public static boolean maintenance = false;

    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();
        reloadConfig();

        PROXY_PORT = BungeeCord.getInstance().config.getListeners().iterator().next().getHost().getPort();
        mysqlHandler = new MySQLHandler(getConfig());
        permissionHandler = new PermissionHandler();
        proxyData = new ProxyData(getMySQLHandler());

        new Thread() {
            @Override
            public void run() {

                nettyClient = new NettyClient(8000);
                getNettyClient().setupServerConnection();

                super.run();
            }
        }.start();

        ProxyData.createTable(TablesType.PROXY_DATA);

        proxyData.loadData();

        // Registriert alle Events
        ProxyServer.getInstance().getPluginManager().registerListener(this, new PingListener());
        ProxyServer.getInstance().getPluginManager().registerListener(this, new JQListener());

        // Registriert alle Befehle
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ServerCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PexCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new SendCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new FindCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new ListCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new AlertCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MaintenanceCommand());
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new MotdCommand());

        ProxyServer.getInstance().getScheduler().schedule(this, new ServerUpdater(), 0, 10, TimeUnit.MINUTES);

        super.onEnable();
    }

    @Override
    public void onDisable() {

        getNettyClient().sendString("unregisterproxy " + PROXY_PORT, getNettyClient().getChannel());

        nettyClient = null;
        instance = null;

        super.onDisable();
    }

    public static BattleProxy getInstance() {
        return instance;
    }

    public NettyClient getNettyClient() {
        return nettyClient;
    }

    public MySQLHandler getMySQLHandler() {
        return mysqlHandler;
    }

    public PermissionHandler getPermissionHandler() {
        return permissionHandler;
    }

    public ProxyData getProxyData() {
        return proxyData;
    }

    public int getProxyPort() {
        return PROXY_PORT;
    }

    public Configuration getConfig() {
        return configuration;
    }

    public void reloadConfig() {
        try {
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(BattleProxy.getInstance().getDataFolder(), "config.yml"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to load configuration", e);
        }
    }

    public void saveConfig() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.getConfig(), new File(BattleProxy.getInstance().getDataFolder(), "config.yml"));
        } catch (IOException e) {
            throw new RuntimeException("Unable to save configuration", e);
        }
    }

    public void saveDefaultConfig() {
        if (!BattleProxy.getInstance().getDataFolder().exists()) {
            BattleProxy.getInstance().getDataFolder().mkdir();
        }

        final File configFile = new File(BattleProxy.getInstance().getDataFolder(), "config.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                Throwable t = null;
                try {
                    final InputStream is = BattleProxy.getInstance().getResourceAsStream("config.yml");
                    try {
                        final OutputStream os = new FileOutputStream(configFile);
                        try {
                            ByteStreams.copy(is, os);
                        } finally {
                            if (os != null) {
                                os.close();
                            }
                        }
                        if (is != null) {
                            is.close();
                        }
                    } finally {
                        if (t == null) {
                            Throwable t2 = null;
                            t = t2;
                        } else {
                            final Throwable t2 = null;
                            if (t != t2) {
                                t.addSuppressed(t2);
                            }
                        }
                        if (is != null) {
                            is.close();
                        }
                    }
                } finally {
                    if (t == null) {
                        Throwable t3 = null;
                        t = t3;
                    } else {
                        Throwable t3 = null;
                        if (t != t3) {
                            t.addSuppressed(t3);
                        }
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
    }
}
