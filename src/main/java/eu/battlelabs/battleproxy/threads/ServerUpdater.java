package eu.battlelabs.battleproxy.threads;

import eu.battlelabs.battleproxy.BattleProxy;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ServerUpdater implements Runnable {

    @Override
    public void run() {
        try {
            PreparedStatement preparedStatement = BattleProxy.getInstance().getMySQLHandler().getConnection().prepareStatement("SELECT * FROM " + "battlelabs_proxy_data");
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                BattleProxy.maintenance = resultSet.getBoolean("maintenance");
                BattleProxy.motd = resultSet.getString("motd");
            }

            preparedStatement.close();
            resultSet.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
